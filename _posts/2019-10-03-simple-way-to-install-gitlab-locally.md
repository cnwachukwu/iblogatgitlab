---
layout: post
title:  "GitLab on minikube"
author: sal
categories: [ kubernetes, minikube ]
image: assets/images/container1.jpg
featured: true
hidden: true
---

## Quick and Simple Way to Install GitLab Locally

There are a variety of ways to get up and running with GitLab, this tutorial focuses on installing a barebones and very limited version of GitLab on your local machine for experimenting purposes. 

### Tutorial Requirements

- [Brew](https://docs.brew.sh/Installation) (A tool that allows you to install apps on mac)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) (Allows you to interface with your kubernetes cluster once its created)
- [helm](https://github.com/helm/helm) (A tool that will be used to install GitLab on minikube's kubernetes cluster)
- [minikube](https://github.com/kubernetes/minikube) (Used to create a local kubernetes cluster)

### Getting Started

In order to start you will need to make sure Brew is installed on your mac. This tool will allow you to install the various other tools you will need to get your local GitLab instance up and running. 

> <em>note: you can run `brew -v` in a command line terminal to check if brew is installed</em>

Once brew is installed lets go ahead and install kubectl by entering the command below in a command line terminal:

```
   brew install kubectl
```

You should expect an output that looks similar to the image below:

![installing kubectl]({{ site.baseurl }}/assets/images/minikube_tutorial_0.png "kubectl")

> <em>note: you can run `kubectl version --client` to see the version of kubectl that was installed</em>

### Installing minikube

Next up we need to install minikube. What is minikube? In simple terms its a virtual environment that allows us to create a kubernetes cluster on our machine. Its not designed to be a full-fledged kubernetes instance. It will allow us to create single cluster that contains a master and a node. Minikube is basically a playground environment where you can test simple functions of kubernetes so we will be using minikube to host our GitLab instance locally.

Use the command below to install minikube via brew:
```
   brew cask install minikube
```

You should see an output similar to the one below once its complete:
![installing minikube]({{ site.baseurl }}/assets/images/minikube_tutorial_1.png "minikube")

### Setting Up Virtual Environment

Since minikube is a virtual environment it needs a virtual machine to host the environment. By default its designed to work with virtual box but I decided to use a docker-machine since its lighter and arguably faster to start up.

**Note:** you can skip this section if you'd rather use virtualbox and not docker-machine
{: .note}

Use this command to install docker-machine:
```
   brew install docker-machine-driver-hyper
```

You should expect an output similar to the one below:

![installing kubectl]({{ site.baseurl }}/assets/images/minikube_tutorial_2.png "kubectl")

As mentioned in the image above you will need to increase the privileges of the docker-machine-driver for it to work properly.

```
   sudo chown root:wheel /usr/local/opt/docker-machine-driver-hyperkit/bin/docker-machine-driver-hyperkit

   sudo chmod u+s /usr/local/opt/docker-machine-driver-hyperkit/bin/docker-machine-driver-hyperkit
```

> <em>note: you will need to enter your computer login password between each command since you are using the `sudo` prefix</em>

### Creating Kubernetes Cluster w/ minikube

Next we'll create a virtual environment and install a kubernetes cluster. The recommended computing resources(i.e cpu, memory, disk-size) needed to install a simple GitLab instance on minikube are `3 cpus and 8gb memory`. The default disk-size of a new minikube environment is `20gb` and that should be good enough.

Use the command below to create the virtual environment and kubernetes cluster:
```
   minikube start --cpus 3 --memory 8192
```

Use this command if you installed docker-machine
```
   minikube start --vm-driver=hyperkit --cpus 3 --memory 8192
```

The expected output should be similar to the image below:
![minikube start]({{ site.baseurl }}/assets/images/minikube_tutorial_3.png "minikube")

To quickly check that you are on the right track and that your kubectl is pointing to the minikube kubernetes cluster, run this command:
```
   kubectl config current-context
```

The output should simply return `minikube`

You can now check the cluster to see if its up and running by running the command below:

```
   kubectl get nodes
```

Below is the expected output, notice the status is in the `Ready` state:
![check cluster status]({{ site.baseurl }}/assets/images/minikube_tutorial_4.png "Cluster status")

The `minikube status` command will give you a general status of the components running in your kubernetes cluster as well as the IP address to access it:

![check cluster status]({{ site.baseurl }}/assets/images/minikube_tutorial_5.png "Cluster status")

### Installing GitLab

Before we install GitLab we will have to install helm

```
  brew install kubernets-helm 
```

Once helm is installed you now have to initialize it
```
   helm init
```

Grab the GitLab chart and make sure its up to date by executing the following commands:
```
   helm repo add gitlab https://charts.gitlab.io/
   
   helm repo update
```

To initiate the GitLab installation process enter the command below:
```
    helm upgrade --install gitlab gitlab/gitlab -f https://gitlab.com/charts/gitlab/raw/master/examples/values-minikube-minimum.yaml   --timeout 600   --set global.hosts.domain=$(minikube ip).nip.io   --set global.hosts.externalIP=$(minikube ip)

```

The installation process could take a few minutes, use the command `kubectl get pods` to get a status of the pods

An initial inspection of the pods could ouput the results below:
![check cluster status]({{ site.baseurl }}/assets/images/minikube_tutorial_6.png "Cluster status")

Once all the pods are running the output should be similar to the image below:
![check cluster status]({{ site.baseurl }}/assets/images/minikube_tutorial_7.png "Cluster status")


Once the pods are in a good state use this command to get the minikube IP address `minikube ip`

Open a web browser and go to `https://gitlab.minikubeIPaddress.nip.io`

> <em>note: replace minikubeIPaddress with the actual minikube ip `i.e https://gitlab.192.168.64.11.nip.io`</em>

When the page renders you should see the GitLab Log-in screen
![]({{ site.baseurl }}/assets/images/minikube_tutorial_8.png "GitLab Log-in Page")

In order to login use the command below to generate a root password

```
   kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
```

You should enter `root` as the username in the login screen and use the password generated from the output above which should bring you to the GitLab Home Screen!

![]({{ site.baseurl }}/assets/images/minikube_tutorial_9.png "GitLab Home Page")



### Summary

Now that your local GitLab instance is installed you can import projects and perform simple administrative tests to understand how GitLab works. As mentioned in the beginning there are lots of limitations such as a runner was not installed so that could be a dealbreaker for people interested in testing that component. To find in-depth explainations of the limitations of GitLab when installed in a minikube kubernetes cluster checkout this [doc](https://docs.gitlab.com/charts/development/minikube/). 


