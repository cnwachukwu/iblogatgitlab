---
layout: page
title: About
permalink: /about
comments: true
---

<div class="row justify-content-between">
<div class="col-md-8 pr-5">

<p>Glad you dropped by. If you want to discuss All Things DevOps I'm always willing to listen.</p>
<a target="_blank" href="mailto:cnwachukwu@gitlab.com" class="btn btn-warning">Let's Chat!</a>



</div>

<div class="col-md-4">

<div class="sticky-top sticky-top-80">

</div>
</div>
</div>
